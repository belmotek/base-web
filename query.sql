CREATE TABLE "teams" (
	"idx"	INTEGER,
	"name"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("idx" AUTOINCREMENT)
);
CREATE TABLE "users" (
	"idx"	INTEGER,
	"name"	TEXT NOT NULL UNIQUE,
	"hash"	TEXT,
	"perm"	INTEGER,
	"active"	INTEGER,
	"expires"	TEXT,
	"team"	INTEGER,
	"email"	TEXT,
	"remarks"	TEXT,
	FOREIGN KEY("team") REFERENCES "teams"("idx"),
	PRIMARY KEY("idx" AUTOINCREMENT)
);
INSERT INTO "teams" VALUES (1,'Administrators');
INSERT INTO "users" VALUES (1,'admin','$5$pf1234567890x$lkt/455wkFtPt1ZqEmCbKnsLqCm0TJoTY40CFJzf7v3',5,-1,NULL,1,NULL,NULL);
