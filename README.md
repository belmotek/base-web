[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://gitlab.com/belmotek/base-web/LICENSE)

# Base Web

## About Base Web

A project with everything you need to create a web with Gambas3 WebForms. This project has a didactic but, at the same time, functional purpose (not yet), allowing to copy it and then adapt it to every need.

  * Login form, user validation, storage of hash in database not the password
  * Application form, table, treeview
  * Administration form. Add, edit or delete users. Backup and restore database.
  * Configuration file in JSON format
  * Upload and download files.
  * CSS Style sheet
  * Responsive web, desktop & mobile
  * ¿What else?

## How to use

The project is at a very early stage. But it is possible to use it.
It is not necessary to create an installation package as it can be used from the IDE itself. At the moment it is only a didactic project.
Everything is prepared to display a Sqlite3 database in the home directory the first time you try to log in to the system in the login form. This database contains two tables, users and type of users.

  * user : admin
  * password: 1234

The schema for creating the table is in ./query.sql.
The table does not store the password but a hash generated with a passphrase and a password.

### Dependencies
  * Gambas IDE
  * Sqlite3

## Some files

In the local directoriy "/home/USER/.config/webapp/",  there are some intresting files to see:

  * config.json - Its contains all the info related to the connection
  * data.sqlite - its the database file (sqlite3)
  * schema.json - the database schema

In /tmp/

  * webapp.log

Note: Sometimes is needed to delete the dir: /tmp/gambas.####/ where #### is the user id number

## Classes

In the project directory there are some classes intresting, I hope :-) 

  * Config.class - it manage all the fix config for the app, is veri easy to add variables there.
  * Data.class - it manage all related to database, insert, delete, alter, create, query, etc.
  * JSON.class - it extends JSON class from gb.util-web

## Deploy on Apache server Debian 11 (stable)

### Install database engine
  * sudo apt update
  * sudo apt-get install mysql-server (MySQL database engine)
  * sudo apt-get install mariadb-server (MariaDB database engine)
  * sudo apt install postgresql postgresql-contrib (PosgreSQL database engine)
### Install Gambas3
  * sudo apt update
  * sudo apt -t bullseye-backports install gambas3-gb-web-gui gambas3-gb-crypt gambas3-gb-db-mysql gambas3-gb-db-sqlite3 gambas3-gb-db gambas3-gb-image-io gambas3-gb-image gambas3-gb-pcre gambas3-gb-signal gambas3-gb-util-web gambas3-gb-util gambas3-gb-web-gui gambas3-gb-web gambas3-runtime gambas3-scripter

Note: If the web app finally dont work, look at the Apache log file, it usually shows the name of the packages that are missing.

### Install Apache2
  * sudo apt install apache2
  * sudo nano /etc/apache2/conf-available/serve-cgi-bin.conf

``` 
    <IfModule mod_alias.c>
    <IfModule mod_cgi.c>
    Define ENABLE_USR_LIB_CGI_BIN
    </IfModule>
    <IfModule mod_cgid.c>
    Define ENABLE_USR_LIB_CGI_BIN
    </IfModule>
    <IfDefine ENABLE_USR_LIB_CGI_BIN>
    ScriptAlias /cgi-bin/ /var/www/cgi-bin
    <Directory "/var/www/cgi-bin">
    AllowOverride None
    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
    AddHandler cgi-script .cgi .py .gb
    Require all granted
    </Directory>
    </IfDefine>
    </IfModule>
```

  * sudo a2enmod cgi
  * sudo service apache2 restart
  * sudo systemctl restart apache2
  * sudo systemctl start apache2

#### Apache log file
This file is crucial at the beginning, when trying to deploy an app.gambas, but probably later on it will be too, but it's just a hunch.
When I copied the myweb.gambas into the Apache server directory and did everything "correctly" the application didn't work, so I went to the apache log file and it told me that I didn't have some components installed. I proceeded to install them and then everything worked.



### Deploy the application
  * cp /home/user/proyectoweb.gambas /usr/lib/cgi-bin/
  * sudo chmod +x -R /usr/lib/cgi-bin/
### Access to the web in a web browser
  * On the same computer > http://localhost/cgi-bin/webproject.gambas
  * On another computer > http://ip-computer/cgi-bin/webproject.gambas



## Authors

Martín Belmonte

## Contact

If you have questions, have found bugs or have ideas to implement, feel free to contact me at the email address below.

[info@belmotek.net](info@belmotek.net)


## Acknowledgment

 * Benoît Minisini,  for making gambas
 * The Gambas farm users in particular to the autors of  WebLogin, WebSiteTest and WebDemo.
 * The gambas-es users, in particular to those who shared web application code with WebForms.
 * The international mail list of gambas users.
 * To the debian gambas team for doing the backports for the stable release.

## License

This source code is licensed under GPL v3
